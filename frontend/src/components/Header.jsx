import { useState } from "react";
import { Link } from "react-router-dom";

const Header = () => {
  const [drawer, setDrawer] = useState(false);
  return (
    <>
      <div
        className={`bg-slate-50 absolute top-0 right-0 w-2/5 h-full md:hidden shadow-lg ${
          drawer ? "translate-x-0" : "translate-x-full"
        } transition-all duration-300 `}
      >
          
        <button onClick={() => setDrawer(false)}>
        <span
          className="text-3xl material-symbols-outlined absolute right-7 top-2"
        >
          close
        </span>
        </button>
        <ul className="flex flex-col items-center font-semibold text-sm text-black my-16">
          <Link  className=" border-b border-t w-full text-center py-4" to={"/home"}>Home</Link>
          <Link  className=" border-b w-full text-center py-4 " to={""}>Browse</Link>
          <Link  className=" border-b w-full text-center py-4 " to={""}>Library</Link>
          <Link  className=" border-b w-full text-center py-4 " to={""}>LogIn</Link>
        </ul>
      </div>

      <header className="shadow-lg h-11 px-8 flex justify-between items-center">
        <div className="flex gap-10">
          <div className="flex items-center text-xl text-black font-bold">
            <img className="h-6 " src="/images/logo.png" alt="spotify-logo" />
            <span className=" font-normal text-lg">Tune Tide</span>
          </div>
          <div className="flex gap-2 bg-gray-200 rounded-xl w-[11rem] sm:w-[20rem] sm:px-4 py-1">
            <img src="/icons/search.svg" alt="" />
            <input
              className=" border-transparent outline-none bg-transparent"
              type="text"
              placeholder="Search"
            />
          </div>
        </div>
        <div className="flex items-center">
          <nav className="hidden md:block">
            <ul className="flex gap-4 items-center font-semibold text-sm text-black">
              <Link to={"/"}>Home</Link>
              <Link to={""}>Browse</Link>
              <Link to={""}>Library</Link>
              <Link to={"/login"}>LogIn</Link>
            </ul>
          </nav>
          <button onClick={() => setDrawer(true)}>
            <span className="material-symbols-outlined md:hidden">menu</span>
          </button>
        </div>
      </header>
    </>
  );
};

export default Header;
