import axios from "axios";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";

const SignupPage = () => {
    const [error, setError] = useState("")
    const navigate = useNavigate()
  const handleFormSubmit = async (e) => {
    e.preventDefault();
    const form = e.target;

    const userName = form["userName"].value;
    const email = form["email"].value;
    const password = form["password"].value;
    console.log(password,email,userName);
    try{
       const res = await axios.post(
            "http://localhost:3000/users/signup",
            {
              userName,
              email,
              password,
            },
            { withCredentials: true }
          )
          console.log(res)
          if(res.data.errorMessage){
            setError(res.data.errorMessage)
          }
          if(res.data.user){
            navigate('/login')
          }
    } catch(error){
        console.log(error)
        setError(error.response.data.message)
    }
  };
  return (
    <div className="flex flex-col items-center  justify-center h-screen">
      <div className="flex justify-between items-center lg:max-w-[1024px] shadow-lg lg:w-full">
        <div className="flex px-20 py-20 flex-col items-center justify-center">
          <h1 className="uppercase text-4xl font-semibold">Welcome Back</h1>
          <p className="text-gray-500 text-xs mt-2 mb-10">
            Welcome back! Please enter your details.
          </p>
          <form onChange={()=>setError("")} onSubmit={handleFormSubmit} className="flex flex-col w-full">
            <div className="flex flex-col text-sm mb-4">
              <label htmlFor="email">UserName</label>
              <input
                className="p-2 mt-2 border rounded-xl shadow placeholder:text-xs"
                type="userName"
                name="userName"
                id="userName"
                placeholder="Enter UserName"
              />
            </div>
            <div className="flex flex-col text-sm mb-4">
              <label htmlFor="email">Email</label>
              <input
                className="p-2 mt-2 border rounded-xl shadow placeholder:text-xs"
                type="email"
                name="email"
                id="email"
                placeholder="Enter your email"
              />
            </div>
            <div className="flex flex-col text-sm mb-4">
              <label htmlFor="password">Password</label>
              <input
                className="p-2 mt-2 border rounded-xl shadow placeholder:text-xs"
                type="password"
                name="password"
                id="password"
                placeholder="*********"
              />
            </div>
                {error&&<span className="text-xs bg-red-100 py-2 px-1 text-center text-red-600 rounded shadow-md">{error}</span>}
            <button
              type="submit"
              className=" text-white mt-6 shadow-md text-sm bg-red-500/90 rounded-xl p-2 hover:opacity-90 transition-all duration-300 hover:uppercase"
            >
              Sign Up
            </button>
            <button className="flex shadow-md justify-center items-center mt-2 border rounded-xl text-sm p-2 gap-2">
              <img className="w-5" src="/icons/google.png" alt="" />
              Sign Up with Google
            </button>
            <p className="text-xs text-center mt-4">
              {" "}
              Already have an account?{" "}
              <Link to={"/login"} className="text-red-400">
                Login
              </Link>{" "}
            </p>
          </form>
        </div>
        <div className="hidden lg:block">
          <img
            className="h-fit"
            src="/images/Login.png"
            alt="Login page cover"
          />
        </div>
      </div>
    </div>
  );
};

export default SignupPage;
