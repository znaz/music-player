import { Outlet } from "react-router-dom";
import Header from '../components/Header'

const rootLayout = () => {
  return (
    <>
      <Header/>
      <Outlet />
    </>
  );
};

export default rootLayout;
