import { Link } from "react-router-dom"

const HomePage = () => {
  return (
    <main className="container mx-auto max-w-[1200px]">
        <section>
            <img src="/images/home.png" className="w-full" alt="" />
        </section>
        <section>
            <h2>Featured</h2>
            <ul>
                <Link>
                    <img src="" alt="" />
                    <h3></h3>
                    <span></span>
                </Link>
                <Link></Link>
                <Link></Link>
                <Link></Link>
                <Link></Link>
                <Link></Link>
                <Link></Link>
                <Link></Link>
                <Link></Link>
            </ul>
        </section>
    </main>
  )
}

export default HomePage