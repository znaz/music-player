const express = require("express");
const router = express.Router();
const songController = require("../Controllers/songController");

router.post("/add-song", songController.createSong);


module.exports = router;
