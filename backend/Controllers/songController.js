const {Song, validate} = require("../Models/song")

const createSong = async (req, res, next)=>{
    try{
    const { error } = validate(req.body);
    if (error) {
        return res.status(400).send({ message: error.details[0].message });
      }
      const song = new Song(req.body)
      await song.save()
      res.status(201).json({song,message:"Song created"})
    }
    catch(err){
        console.log(err)
    }
}

const getAllSongs = async(req,res,next)=> {
    try{
        const songs = await Song.find()
        res.status(200).json(songs)
    }
    catch(err){
        console.log(err)
    }
}

module.exports = { createSong, getAllSongs };
