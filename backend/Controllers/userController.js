const { User, validate } = require("../Models/user");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const signUp = async (req, res, next) => {
  try {
    const { error } = validate(req.body);

    if (error) {
      console.log(error.details[0].message)
      return res.send({ errorMessage: error.details[0].message });
    }

    const user = await User.findOne({ email: req.body.email });
    if (user) {
      return res
        .status(403)
        .send({ message: "User with given mail id already exists" });
    }

    const saltRounds = 10;

    const hashPassword = bcrypt.hashSync(req.body.password, saltRounds);

    const newUser = new User({
      ...req.body,
      password: hashPassword,
    });
    await newUser.save();

    res.status(201).json({ user: newUser, message: "User created succefully" });
  
    next()
  } catch (err) {
    console.error(err);
    res.status(500).send("Error Occurred");
  }
};

const logIn = async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!user) {
      return res.status(400).send({ message: "Invalid email or password" });
    }
    const checkPassword = bcrypt.compareSync(req.body.password, user.password);
    if(!checkPassword){
      return res.status(400).send({message: "Invalid email or password"})
    }
    const token = jwt.sign({ id: user._id ,name: user.userName }, process.env.JWT_SECRET);
  
    res.cookie("token", token, {
      withCredentials: true,
      httpOnly: false,
    });

    res.status(201).json({user,message : "Logging in please wait......"})

    next()
  } catch (err) {
    console.error(err);
    res.status(500).send("Error Occurred");
  }
};

module.exports = { signUp, logIn };
