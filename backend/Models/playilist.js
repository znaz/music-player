const Joi = require("joi");
const mongoose = require("mongoose");

const playilistSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    user: {
      type: mongoose.ObjectId,
      ref: 'user',
      required: true,
    },
    desc: {
      type: String,
    },
    songs: {
      type: Array,
      default:[],
    },
    image: {
      type: String,
      required: true,
    }
}
);

const validate = (playilist) => {
  const schema = Joi.object({
    name: Joi.string().required(),
    user: Joi.string().allow(""),
    songs: Joi.array().items(Joi.string()),
    image: Joi.string().allow("")
  });
  return schema.validate(playilist);
};
const Playilist = mongoose.model("playilist", playilistSchema);

module.exports = { Playilist, validate };
