const Joi = require("joi");
const passwordComplexity = require("joi-password-complexity");
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    userName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    likedSongs: {
      type: [String],
      default: [],
    },
    playlists: {
      type: [String],
      default: [],
    },
    isAdmin: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

const validate = (user) => {
  const schema = Joi.object({
    userName: Joi.string().min(6).max(10).required(),
    email: Joi.string().email().required(0),
    password: passwordComplexity().required(),
  });
  return schema.validate(user);
};
const User = mongoose.model("user", userSchema);

module.exports = { User, validate };
