const express = require("express");
const app = express();
require("dotenv").config();
require("express-async-errors");
const port = process.env.PORT || 3000;
const connectDb = require("./Connection/database");
const cors = require("cors");
const cookieParser = require("cookie-parser");

app.use(
  cors({
    origin: ["http://localhost:5173"],
    methods: ["GET", "POST","PUT", "PATCH", "DELETE"],
    credentials: true,
  })
);
app.use(cookieParser());
app.use(express.json());

const Users = require("./Routes/user");
const Songs = require("./Routes/song")

app.use("/users", Users);
app.use("/songs", Songs)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
