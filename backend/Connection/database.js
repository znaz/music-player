const mongoose = require("mongoose");
require("dotenv").config();

const MongoUrl = process.env.DB_URL
connectDb()
  .catch((err) => console.log(err));

async function connectDb() {
  try{
    await mongoose.connect(MongoUrl);
  console.log("db connected")

  } catch(err){
    console.log(err)
  }
}

module.exports = connectDb